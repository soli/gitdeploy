# gitdeploy

Using git for automated deployments.

## Usage

```

$deploy = new Deploy('/var/www');
$deploy->execute();

```
